FROM node:8-alpine as base
ADD default.commitlint.config.js /etc/commitlint/default/commitlint.config.js
ADD ./commitlintit /usr/local/bin/commitlintit
ADD ./run_test /usr/local/bin/run_test
ADD ./tests/ /workdir/tests/
RUN npm i -g commitlint js-beautify jslint && \
    apk add --no-cache bash git openssh-client rsync && \
    chmod u+x /usr/local/bin/commitlintit && \
    chmod u+x /usr/local/bin/run_test && \
    mkdir -p /builds/;
WORKDIR /workdir
