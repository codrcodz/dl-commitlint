#!/bin/bash

# shellcheck disable=SC2181

check_script_deps() {
  echo -e "\n[INFO] commitlintit | Ensuring deps are installed.";
  DEPS="git ssh-keygen ssh-keyscan rsync mkdir cp commitlint js-beautify jslint"
  for DEP in ${DEPS};
  do
    command -v "${DEP}" &>/dev/null || \
    { echo -e "\n[FAIL] commitlintit | Dep (${DEP}) not installed; exiting." 1>&2; exit 11; };
  done; 
};

get_upstream_url() {
  if [[ "${UPSTREAM_URL}" == "" ]]; then
    UPSTREAM_CHECK="$(git remote get-url upstream 2>/dev/null)";
    UPSTREAM_URL="${UPSTREAM_CHECK}";
  fi; 
  if [[ "${UPSTREAM_URL}" == "" ]]; then
    UPSTREAM_URL="${CI_MERGE_REQUEST_PROJECT_URL}.git";
  fi;
  if [[ "${UPSTREAM_URL}" == ".git" ]]; then
    UPSTREAM_URL="${CI_PROJECT_URL}.git";
  fi;
  if [[ "${UPSTREAM_URL}" == ".git" ]]; then
    echo -e "\n[FAIL] commitlintit | Unable to determine value for UPSTREAM_URL; exiting." 1>&2;
    exit 21;
  fi;
};

parse_upstream_url() {
  echo -e "\n[INFO] commitlintit | Parsing git URL for 'upstream' remote (${UPSTREAM_URL}) for protocol.";
  case "${UPSTREAM_URL%:*}" in
    *@*|ssh|rsync)
      UPSTREAM_URL_PROTO="ssh";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL#*@}";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL_NOPROTO#rsync://}";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL_NOPROTO#ssh://}";
      UPSTREAM_URL_HOST="${UPSTREAM_URL_NOPROTO%%:*}";
      UPSTREAM_URL_TAIL="${UPSTREAM_URL_NOPROTO##*:}";
      ;;
    https)
      UPSTREAM_URL_PROTO="https";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL#https://}";
      UPSTREAM_URL_HOST="${UPSTREAM_URL_NOPROTO%%/*}";
      UPSTREAM_URL_TAIL="${UPSTREAM_URL_NOPROTO##*/}";
      ;;
    http)
      UPSTREAM_URL_PROTO="http";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL#http://}";
      UPSTREAM_URL_HOST="${UPSTREAM_URL_NOPROTO%%/*}";
      UPSTREAM_URL_TAIL="${UPSTREAM_URL_NOPROTO##*/}";
      ;;
    git)
      UPSTREAM_URL_PROTO="git";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL#git://}";
      UPSTREAM_URL_HOST="${UPSTREAM_URL_NOPROTO%%/*}";
      UPSTREAM_URL_TAIL="${UPSTREAM_URL_NOPROTO##*/}";
      ;;
    file)
      echo -e "\n[INFO] commitlintit | Git URL for 'upstream' remote (${UPSTREAM_URL}) appears to be file protocol URL.";
      UPSTREAM_URL_PROTO="file";
      UPSTREAM_URL_NOPROTO="${UPSTREAM_URL#file://}";
      ;;
    *)
      echo -e "\n[INFO] commitlintit | Recognized URL formats include: 'ssh:*', 'user@host:*', 'git:*', 'http:*', and 'https:*'.";
      echo -e "\n[FAIL] commitlintit | URL for 'upstream' remote either invalid or unrecognized; exiting." 1>&2;
      exit 31;
      ;; 
  esac;
  echo -e "\n[INFO] commitlintit | Git URL for 'upstream' remote appears to be ${UPSTREAM_URL_PROTO} protocol URL.";
};

add_upstream_as_remote() {
  echo -e "\n[INFO] commitlintit | Ensuring git fetch URL (${UPSTREAM_URL}) associated with 'upstream' remote.";
  git remote add upstream "${UPSTREAM_URL}" || \
  { \
    if [[ "$(git remote get-url upstream)" != "${UPSTREAM_URL}" ]]; then
      echo -e "\n[WARN] commitlintit | Adding 'upstream' remote failed." 1>&2;
      echo -e "\n[INFO] commitlintit | Remote 'upstream' already exists; making backup and replacing fetch URL." 1>&2;
      RANNUM="${RANDOM}";
      git remote add "upstreambackup-${RANNUM}" "$(git remote get-url upstream)" || \
      { echo -e "\n[FAIL] commitlintit | Failed to backup 'upstream' to 'upstreambackup-${RANNUM}; exiting." 1>&2; exit 41; };
      { \
	git remote set-url upstream "${UPSTREAM_URL}" && \
	git remote set-url --push upstream "$(git remote get-url --push upstreambackup-${RANNUM})";
      } || \
      { echo -e "\n[FAIL] commitlintit | Failed to set new fetch URL for 'upstream' remote; exiting." 1>&2; exit 51; };
    else
      echo -e "\n[INFO] commitlintit | Remote 'upstream' already existed with same fetch URL (${UPSTREAM_URL}).";
    fi;
  };
  echo -e "\n[INFO] commitlintit | Ensured git fetch URL (${UPSTREAM_URL}) associated with 'upstream' remote.";
};

fetch_upstream() {
  if [[ "${UPSTREAM_URL_PROTO}" == "ssh" ]]; then
    echo -e "\n[INFO] commitlintit | Ensuring hostkey for 'upstream' remote host (${UPSTREAM_URL_HOST}) added to known_hosts file.";
    ssh-keygen -F "${UPSTREAM_URL_HOST}" || \
    { \
      echo -e "\n[INFO] commitlintit | Hostkey not present; retrieving."; \
      mkdir -p ~/.ssh/ && \
      ssh-keyscan -t rsa "${UPSTREAM_URL_HOST}" >> ~/.ssh/known_hosts;
    } || \
    { echo -e "\n[FAIL] commitlintit | Keyscan on 'upstream' remote host (${UPSTREAM_URL_HOST}) failed; exiting." 1>&2; exit 61; };
    echo -e "\n[INFO] commitlintit | Ensured hostkey for 'upstream' remote host (${UPSTREAM_URL_HOST}) is in known_hosts file.";
    echo -e "\n[INFO] commitlintit | Attempting to fetch 'upstream' remote from git URL (${UPSTREAM_URL}).";
  fi;
  git fetch upstream || git fetch upstream || git fetch upstream || \
  { \
    echo -e "\n[WARN] commitlintit | Failed to fetch 'upstream' remote from git URL (${UPSTREAM_URL}); returning." 1>&2;
    FAIL_COUNT="$((FAIL_COUNT+1))";
    return 1;
  };
  echo -e "\n[INFO] commitlintit | Successfully fetched 'upstream' remote from git URL (${UPSTREAM_URL}).";
  UPSTREAM_FETCHED="true";
};

convert_upstream_url() {
  echo -e "\n[INFO] commitlintit | Converting git URL protocol from '$UPSTREAM_URL_PROTO' to another format before retrying fetch.";
  case "${UPSTREAM_URL_PROTO}" in
    ssh)
      UPSTREAM_URL="https://${UPSTREAM_URL_HOST}/${UPSTREAM_URL_TAIL}";
      UPSTREAM_URL_PROTO="https";
      ;;
    https)
      UPSTREAM_URL="http://${UPSTREAM_URL_HOST}/${UPSTREAM_URL_TAIL}";
      UPSTREAM_URL_PROTO="http";
      ;;
    http)
      UPSTREAM_URL="git@${UPSTREAM_URL_HOST}:${UPSTREAM_URL_TAIL}";
      UPSTREAM_URL_PROTO="ssh";
      ;;
    *)
      echo -e "\n[FAIL] commitlintit | Unrecognized git URL protocol ($UPSTREAM_URL_PROTO); exiting." 1>&2;
      exit 71;
      ;;
  esac;  
  echo -e "\n[INFO] commitlintit | Converted git URL protocol to '$UPSTREAM_URL_PROTO'.";
};

parse_head_commit_sha() {
  if [[ "${COMMIT_SHA}" == "" ]]; then
    COMMIT_SHA="${CI_COMMIT_SHA}";
  fi;
  PARSED_COMMIT_SHA="$(git rev-parse --verify HEAD 2>/dev/null)";
  if [[ "${COMMIT_SHA}" == "" ]] && [[ "${PARSED_COMMIT_SHA}" != "" ]]; then
    COMMIT_SHA="${PARSED_COMMIT_SHA}";
  elif [[ "${COMMIT_SHA}" == "" ]] && [[ "${PARSED_COMMIT_SHA}" == "" ]]; then
    echo -e "\n[INFO] commitlintit | Failed to determine SHA of latest git commit in git log; exiting."
    exit 1;
  fi;
};

lint_config() {
  echo -e "\n[INFO] commitlintit | Ensuring config file (${COMMITLINT_CONFIG}) exists and is a normal file."
  if [[ ! -f "${COMMITLINT_CONFIG}" ]]; then
    echo -e "\n[WARN] commitlintit | Config file (${COMMITLINT_CONFIG}) does not exist, or is not a normal file; returning." 1>&2;
    return 1;
  fi;
  echo -e "\n[INFO] commitlintit | Linting config file (${COMMITLINT_CONFIG}).";
  jslint "${COMMITLINT_CONFIG}" || \
  { echo -e "\n[WARN] commitlintit | Initial linting of config file (${COMMITLINT_CONFIG}) failed; backing up file and attempting to fix.";
    { cp "${COMMITLINT_CONFIG}" "${COMMITLINT_CONFIG}.bak" && \
      js-beautify --type js -r "${COMMITLINT_CONFIG}" && \
      jslint "${COMMITLINT_CONFIG}"; \
    } || \
    { echo -e "\n[FAIL] commitlintit | Failed to automatically remediate linting issue with config file (${COMMITLINT_CONFIG})." 2>&1; \
      exit 81; \
    };
  };
  echo -e "\n[INFO] commitlintit | Successfully validated that config file (${COMMITLINT_CONFIG}) contains valid javascript; proceeding.";
};

select_config_source() {
  echo -e "\n[INFO] commitlintit | Determining config source for commitlint.";
  case "${COMMITLINT_CONFIG}" in
    "")
      echo -e "\n[INFO] commitlintit | No valid configuration file location is set; attempting to fallback to implicit config file location (${PWD}/commitlint.config.js).";
      export COMMITLINT_CONFIG="${PWD}/commitlint.config.js";
      select_config_source;
      ;;
    "${PWD}/commitlint.config.js")
      echo -e "\n[INFO] commitlintit | Config file location is set to (${COMMITLINT_CONFIG}).";
      lint_config;
      if [[ "${?}" != "0" ]]; then
        export COMMITLINT_CONFIG="/etc/commitlint/default/commitlint.config.js";
	echo -e "\n[INFO] commitlintit | Attempting fallback to default config file (${COMMITLINT_CONFIG}).";
	select_config_source;
      fi;
      ;;
    "/etc/commitlint/default/commitlint.config.js")
      echo -e "\n[INFO] commitlintit | Config file location is set to (${COMMITLINT_CONFIG}).";
      lint_config || \
      { echo -e "\n[FAIL] commitlintit | No other config file location fallbacks remaining; exiting." 2>&1; \
	exit 91; \
      };
      ;;
    *)
      echo -e "\n[INFO] commitlintit | Config file location is set to (${COMMITLINT_CONFIG}).";
      lint_config || \
      { export COMMITLINT_CONFIG="" && \
	select_config_source; \
      };
      ;;
  esac;
};

lint_commit() {
  if [[ "${TARGET_BRANCH}" == "" ]]; then
    TARGET_BRANCH="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}";
  fi;
  TARGET_BRANCH="${TARGET_BRANCH:=master}";
  echo -e "\n[INFO] commitlintit | Attempting to lint messages of all new commits not already in upstream/${TARGET_BRANCH}."
  echo -e "\n[INFO] commitlintit | Testing the following list of commits:\n";
  echo -e "$(git --no-pager log --pretty=oneline upstream/${TARGET_BRANCH}..HEAD)";
  echo -e "---END-OF-LIST---\n";
  echo -e "Results of commitlint:\n";
  commitlint \
    --from="upstream/${TARGET_BRANCH}" \
    --to="${COMMIT_SHA}" \
    --config="${COMMITLINT_CONFIG}" || \
  { echo -e "\n[FAIL] commitlintit | Commitlint failed; exiting."; exit 1; };
  echo -e "\n[PASS] commitlintit | Commitlint passed; exiting."; 
}; 

main() {
  check_script_deps;
  get_upstream_url;
  parse_upstream_url;
  add_upstream_as_remote;
  FAIL_COUNT="0";
  UPSTREAM_FETCHED="false";
  fetch_upstream;
  until [[ "${FAIL_COUNT}" == "3" ]] || [[ "${UPSTREAM_FETCHED}" == "true" ]]; do
    convert_upstream_url; parse_upstream_url; add_upstream_as_remote; fetch_upstream;
  done;
  if [[ "${UPSTREAM_FETCHED}" == "true" ]]; then
    parse_head_commit_sha; 
    select_config_source;
    lint_commit;
  else
    echo -e "\n[FAIL] commitlintit | Failed to fetch upstream after multiple retries; exiting." 1>&2;
    exit 101;
  fi;
};

main;
