# dl-commitlint

Builds docker container image containing the `commitlint` tool as well as the `commitlintit` wrapper script.

## Purpose

The Docker container built from this repository can be used to perform linting of commit messages based on the AngularJS commit standard located here: [Angular Style commit messages](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)

### Usage

#### GitlabCI Pipeline

The simplest way to begin using this tool suite in a GitlabCI pipeline is to include the pre-built pipeline in your `.gitlab-ci.yml` file like so:

```
include:
  - project: dreamer-labs/repoman/gitlab-ci-templates
    file: /.gitlab-ci-commitchecker.yml
    ref: master
```

The included pipeline looks like this:

```
---

stages:
  - lint

commitlint:
  image: registry.gitlab.com/dreamer-labs/repoman/dl-commitlint:latest
  stage: lint
  script:
    - commitlintit
  only:
    - merge_requests

...

```

The `commitlintit` script does not require any user-provided input when used as part of a GitlabCI pipeline. It is designed to inherit variables from the GitlabCI system.

#### Local Machine

If you would like to pre-validate your commit messages prior to pushing a commit to GitlabCI, you can do that by pulling down the container image in this repository:

```
docker pull registry.gitlab.com/dreamer-labs/repoman/dl-commitlint/image:latest;
docker run -v ${PWD}:/workdir registry.gitlab.com/dreamer-labs/repoman/dl-commitlint/image:latest --entrypoint "bash -c 'cd /workdir && commitlintit'"
```

When running locally, you may encounter one particular type of fatal error that you typically will not see when the container runs in GitlabCI. Specifically:

```
[FAIL] commitlintit | Unable to determine value for UPSTREAM_URL; exiting.
```

This is due to no `upstream` remote being set for the repository. This URL is required in order for `commitlint` to determine which commit messages to parse.
Normally, if the variable is not set by the user (or GitlabCI), it parses it from the environment, but if no remote named "upstream" is assigned, you have to add one like so:

```
git remote add upstream ${UPSTREAM_URL}; # Replace the variable with the git-formatted URL for your upstream repo
git remote -v; # Check it afterwards to make sure upstream is set to what you expect
```

### Development

If you are developing `commitlintit` or the container image for it and have `docker-compose` installed, you can take advantage of the `docker-compose.yml` file to build and run interactively for debugging:

```
docker-compose build --no-cache dl-commitlint
docker-compose run dl-commitlint
```

To run the same test suite located in the `.gitlab-ci.yml` file:

```
docker-compose run ${test_name}
```

See `docker-compose.yml` for test names.

### Advanced Usage and Features

#### Configuring Custom Linting Rules

The linting rules can be configured in a `*.js` file in your repository. This file is optional.
If you opt to specify one, you must provide a value for the `COMMITLINT_CONFIG` variable to the `commitlintit` script inside the container.

The `default.commitlint.config.js` file is an example of a properly formatted file:

```json
module.exports = {
    'rules': {
        'body-leading-blank': [1, 'always'],
        'footer-leading-blank': [1, 'always'],
        'header-max-length': [2, 'always', 72],
        'scope-case': [2, 'always', 'lower-case'],
        'subject-case': [
            0,
            'never', ['sentence-case', 'start-case', 'pascal-case', 'upper-case']
        ],
        'subject-empty': [2, 'never'],
        'subject-full-stop': [2, 'never', '.'],
        'type-case': [2, 'always', 'lower-case'],
        'type-empty': [2, 'never'],
        'type-enum': [
            2,
            'always', [
                'build',
                'chore',
                'ci',
                'docs',
                'feat',
                'fix',
                'perf',
                'refactor',
                'revert',
                'style',
                'test'
            ]
        ]
    }
};
```

If you opt not to use this default file, and specify a custom one, the file will be linted with `jslint` prior to being passed to `commitlint`:

```
[INFO] commitlintit | Ensuring config file (/etc/commitlint/default/commitlint.config.js) exists and is a normal file.

[INFO] commitlintit | Linting config file (/etc/commitlint/default/commitlint.config.js).

/etc/commitlint/default/commitlint.config.js is OK.

[INFO] commitlintit | Successfully validated that config file (/etc/commitlint/default/commitlint.config.js) contains valid javascript; proceeding.

[INFO] commitlintit | Attempting to lint messages of all new commits not already in upstream/master.
```

If the linting initially fails, `js-beautify` will be used to attempt automatic remediation, and then `jslint` will be used to attempt linting again. If this does not fix the issue, `commitlintit` will exit.

#### Configuring a Custom Upstream URL

A custom URL for the "upstream" remote can be configured by passing the `UPSTREAM_URL` variable to the `commitlintit` script in the container.
If one is _not_ passed, the script will attempt to autodetect the URL. If that fails, this message will be displayed:

```
[FAIL] commitlintit | Unable to determine value for UPSTREAM_URL; exiting.
```

This is due to no `upstream` remote being set for the repository in your git config file. This URL is required in order for `commitlint` to determine which commit messages to lint.
Normally, if the variable is not set by the user (or GitlabCI), it parses it from the environment, but if no remote named "upstream" is assigned, you have to add one like so:

```
git remote add upstream ${UPSTREAM_URL}; # Replace the variable with the git-formatted URL for your upstream repo
git remote -v; # Check it afterwards to make sure upstream is set to what you expect
```

If the script is unable to `git fetch` the repo at that URL, it will attempt to replace the fetch URL for upstream with various versions.
For example, here is a typical output for `git remote -v`:

```
origin  git@gitlab.com:some-username-here/dl-commitlint.git (fetch)
origin  git@gitlab.com:some-username-here/dl-commitlint.git (push)
upstream        git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (fetch)
upstream        git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (push)
```

This works great, so long as an authorized ssh key exists on the system attempting the `git fetch` the remote upstream. If one does not, the script will backup the current upstream URL and replace it with an `https` formatted git URL like so:

```
[INFO] commitlintit | Parsing git URL for 'upstream' remote (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) for protocol.

[INFO] commitlintit | Git URL for 'upstream' remote appears to be ssh protocol URL.

[INFO] commitlintit | Ensuring git fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.
fatal: remote upstream already exists.

[INFO] commitlintit | Remote 'upstream' already existed with same fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git).

[INFO] commitlintit | Ensured git fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.

[INFO] commitlintit | Ensuring hostkey for 'upstream' remote host (gitlab.com) added to known_hosts file.
do_known_hosts: hostkeys_foreach failed: No such file or directory

[INFO] commitlintit | Hostkey not present; retrieving.
# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.8

[INFO] commitlintit | Ensured hostkey for 'upstream' remote host (gitlab.com) is in known_hosts file.

[INFO] commitlintit | Attempting to fetch 'upstream' remote from git URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git).
Warning: Permanently added the RSA host key for IP address '35.231.145.151' to the list of known hosts.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

[WARN] commitlintit | Failed to fetch 'upstream' remote from git URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git); returning.

[INFO] commitlintit | Converting git URL protocol from 'ssh' to another format before retrying fetch.

[INFO] commitlintit | Converted git URL protocol to 'https'.

[INFO] commitlintit | Parsing git URL for 'upstream' remote (https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git) for protocol.

[INFO] commitlintit | Git URL for 'upstream' remote appears to be https protocol URL.

[INFO] commitlintit | Ensuring git fetch URL (https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.
fatal: remote upstream already exists.

[WARN] commitlintit | Adding 'upstream' remote failed.

[INFO] commitlintit | Remote 'upstream' already exists; making backup and replacing fetch URL.

[INFO] commitlintit | Ensured git fetch URL (https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.

[INFO] commitlintit | Successfully fetched 'upstream' remote from git URL (https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git).
```

After this, the `git remote -v` command should output something like so:

```
origin  git@gitlab.com:some-username-here/dl-commitlint.git (fetch)
origin  git@gitlab.com:some-username-here/dl-commitlint.git (push)
upstream        https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git (fetch)
upstream        git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (push)
upstreambackup-1654     git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (fetch)
upstreambackup-1654     git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (push)
```

The old `upstream` remote was backed up to `upstreambackup-${some_random_number}` and `upstream` was updated with the `https` version of the same URL.
Had this attempt failed too, the process would have been repeated, replacing the `https` with `http` before retrying the `git fetch`.
Additionally, multiple attempts were made before cutting over to a different URL format. This accounts for minor, intermittent network issues.
The script will only exit with a failure once each URL type is attempted (excluding the `file://` type).

```

If the linting initially fails, `js-beautify` will be used to attempt automatic remediation, and then `jslint` will be used to attempt linting again. If this does not fix the issue, `commitlintit` will exit.

#### Configuring a Custom Upstream URL

A custom URL for the "upstream" remote can be configured by passing the `UPSTREAM_URL` variable to the `commitlintit` script in the container.
If one is _not_ passed, the script will attempt to autodetect the URL. If that fails, this message will be displayed:


```

[FAIL] commitlintit | Unable to determine value for UPSTREAM_URL; exiting.

```

This is due to no `upstream` remote being set for the repository in your git config file. This URL is required in order for `commitlint` to determine which commit messages to lint.
Normally, if the variable is not set by the user (or GitlabCI), it parses it from the environment, but if no remote named "upstream" is assigned, you have to add one like so:

```

git remote add upstream ${UPSTREAM_URL}; # Replace the variable with the git-formatted URL for your upstream repo
git remote -v; # Check it afterwards to make sure upstream is set to what you expect

```

If the script is unable to `git fetch` the repo at that URL, it will attempt to replace the fetch URL for upstream with various versions.
For example, here is a typical output for `git remote -v`:

```

origin  git@gitlab.com:some-username-here/dl-commitlint.git (fetch)
origin  git@gitlab.com:some-username-here/dl-commitlint.git (push)
upstream        git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (fetch)
upstream        git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git (push)

```

This works great, so long as an authorized ssh key exists on the system attempting the `git fetch` the remote upstream. If one does not, the script will backup the current upstream URL and replace it with an `https` formatted git URL like so:

```

[INFO] commitlintit | Parsing git URL for 'upstream' remote (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) for protocol.

[INFO] commitlintit | Git URL for 'upstream' remote appears to be ssh protocol URL.

[INFO] commitlintit | Ensuring git fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.
fatal: remote upstream already exists.

[INFO] commitlintit | Remote 'upstream' already existed with same fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git).

[INFO] commitlintit | Ensured git fetch URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git) associated with 'upstream' remote.

[INFO] commitlintit | Ensuring hostkey for 'upstream' remote host (gitlab.com) added to known_hosts file.
do_known_hosts: hostkeys_foreach failed: No such file or directory

[INFO] commitlintit | Hostkey not present; retrieving.
`# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.8`

[INFO] commitlintit | Ensured hostkey for 'upstream' remote host (gitlab.com) is in known_hosts file.

[INFO] commitlintit | Attempting to fetch 'upstream' remote from git URL (git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git).
Warning: Permanently added the RSA host key for IP address '35.231.145.151' to the list of known hosts.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.

[WARN] commitlintit | Failed to fetch 'upstream' remote from git URL (`git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git`); returning.

[INFO] commitlintit | Converting git URL protocol from 'ssh' to another format before retrying fetch.

[INFO] commitlintit | Converted git URL protocol to 'https'.

[INFO] commitlintit | Parsing git URL for 'upstream' remote (`https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git`) for protocol.

[INFO] commitlintit | Git URL for 'upstream' remote appears to be https protocol URL.

[INFO] commitlintit | Ensuring git fetch URL (`https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git`) associated with 'upstream' remote.
fatal: remote upstream already exists.

[WARN] commitlintit | Adding 'upstream' remote failed.

[INFO] commitlintit | Remote 'upstream' already exists; making backup and replacing fetch URL.

[INFO] commitlintit | Ensured git fetch URL (`https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git`) associated with 'upstream' remote.

[INFO] commitlintit | Successfully fetched 'upstream' remote from git URL (`https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git`).

```

After this, the `git remote -v` command should output something like so:

```

origin  `git@gitlab.com:some-username-here/dl-commitlint.git` (fetch)
origin  `git@gitlab.com:some-username-here/dl-commitlint.git` (push)
upstream        `https://gitlab.com/dreamer-labs/repoman/dl-commitlint.git` (fetch)
upstream        `git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git` (push)
upstreambackup-1654     `git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git` (fetch)
upstreambackup-1654     `git@gitlab.com:dreamer-labs/repoman/dl-commitlint.git` (push)

```

The old `upstream` remote was backed up to `upstreambackup-${some_random_number}` and `upstream` was updated with the `https` version of the same URL.
Had this attempt failed too, the process would have been repeated, replacing the `https` with `http` before retrying the `git fetch`.
Additionally, multiple attempts were made before cutting over to a different URL format. This accounts for minor, intermittent network issues.
The script will only exit with a failure once each URL type is attempted (excluding the `file://` type).

Valid git URL formats supported include:

```

`file://${FILE_PATH}`
`git://${URL}`
`https://${URL}`
`http://${URL}`
`ssh://${URL}`
`rsync://${URL}`
`git@${URL}`

```

